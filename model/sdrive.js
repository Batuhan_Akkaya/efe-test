const mongoose = require("mongoose"),
	Schema = mongoose.Schema;

const driveSchema = new Schema(
	{
		latitude:{
			type:String
		},
		longitude:{
			type:String
		},
		deviceID: {
            type:Number
        },
		speed: {
            type:Number
        }
	},
	{
		timestamps : true
	}
);

module.exports = mongoose.model("drives",driveSchema);