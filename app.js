
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require("mongoose");

const host = "mongodb://efe:efe123@ds163730.mlab.com:63730/drivers";

var indexRouter = require('./routes/index');
var cRouter     = require("./roueter");


var app = express();
//mongo dbye connect oluyoruz.
mongoose.connect(host, (err)=>{console.log('connection error ' , err)});
cRouter(app);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.listen('3001',function(){
    console.log("efes");
});




app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/*const controler = require("./controller/sdrive");
controler.saveDrive({});*/

module.exports = app;
