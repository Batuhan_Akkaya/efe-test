const sdriveController = require("./controller/sdrive"),
    express = require("express");


module.exports = function (app) {
    const apiRoutes = express.Router(),
        sdriveRoute = express.Router();

    apiRoutes.use("/sdrive", sdriveRoute);


     //sdrive router
    sdriveRoute.get("/create",sdriveController.saveDrive);

    app.use("/api",apiRoutes);
};


